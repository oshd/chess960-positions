MASTER= chess960-positions
BIB= refs.bib

## main build rule
$(MASTER).pdf: $(MASTER).tex $(BIB)
	latexmk -outdir=out -xelatex -logfilewarninglist -latexoption="-synctex=1 -output-driver='xdvipdfmx -V 5'" $(MASTER)

clean:
	latexmk -c $(MASTER).tex
