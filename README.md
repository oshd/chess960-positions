chess960-positions
==================
This LaTeX code generates all chess960 positions according to the numbering scheme developed by Reinhard Scharnagl. The data is from [Mark Weeks](http://www.mark-weeks.com/cfaa/chess960/c960strt.htm). I built this on [TeX Live](https://www.tug.org/texlive/), and I've included a simple makefile.

Build
-----
To create the PDF file:

```sh
git clone https://gitlab.com/oshd/chess960-positions.git
cd chess960-positions
make
```
and the PDF file `chess960-positions.pdf` should be in the `out` directory (obviously this won't work without a functioning TeX distribution).
